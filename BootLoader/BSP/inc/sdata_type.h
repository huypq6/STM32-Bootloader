/*
	Author: 		HUYPQ6
	Company:		FPT
	Date Created: 	Jan-18 2017
	File: 			sdata_type.h
	Description:	
*/
#ifndef __S_DATA_TYPE_H_
#define __S_DATA_TYPE_H_

/*Include*/
#include "stm32l4xx_hal.h"

typedef enum {
	S_OK =0,
	S_ERROR
} S_STATUS;

#endif /*__S_DATA_TYPE_H_*/
