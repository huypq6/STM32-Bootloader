/*
	Author: 		HUYPQ6
	Company:		FPT
	Date Created: 	Oct-24 2016
	File: 			sdebuger.h
	Description:	Print debug into Uart
	
	Update history:
*/
#ifndef __S_DEBUGER_H_
#define __S_DEBUGER_H_

/*Include*/
#include "stm32l4xx_hal.h"
#include "sboard_config.h"

#define SDEBUGER_USING_RTOS		0	/*Change to 0 if not using RTOS*/

/*Define UART PORT for debuger*/
#ifndef SDEBUGER_UART_PORT
#define	SDEBUGER_UART_PORT			DEBUG_UART_PORT	/*Cofig in board_config*/
#define SDEBUGER_UART_BAUDRATE		DEBUG_BAUDRATE
#endif

/*Reimplement printf with CMSIS os mutex*/
#if SDEBUGER_USING_RTOS > 0
extern osMutexId debuggerMutexId;
#define s_printf(x) do { \
						osMutexWait(debuggerMutexId, osWaitForever); \
						printf(x); \
						osMutexRelease(debuggerMutexId); \
					} while (0)
#else
#define s_printf(x) do { \
						printf(x); \
					} while (0)		
#endif



/**/
#if SDEBUGER_ENABLE > 0
#define DBG  s_printf
#else
#define DBG(...)
#endif


/*Extern variable*/
extern FILE *dbgstream;
extern UART_HandleTypeDef huartDebug;
				
/*Declare functions*/
HAL_StatusTypeDef initializeDebugger(void);
					
#endif /*__S_DEBUGER_H_*/
