/*
	Author: 		HUYPQ6
	Company:		FPT
	Date Created: 	Oct-24 2016
	File: 			sdebuger.h
	Description:	Print debug into Uart
	
	Update history:
*/
#include "sdebugger.h"



UART_HandleTypeDef huartDebug;

static void initDebuggerGPIO() {
	
}

/*Initlize Debugger via UART*/
HAL_StatusTypeDef initializeDebugger() {
#if SDEBUGER_ENABLE	 > 0
	huartDebug.Instance = SDEBUGER_UART_PORT;
	huartDebug.Init.BaudRate = SDEBUGER_UART_BAUDRATE;
	
	huartDebug.Init.Mode = UART_MODE_TX_RX;
	huartDebug.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huartDebug.Init.OneBitSampling = UART_ONEBIT_SAMPLING_DISABLED;
	huartDebug.Init.StopBits = UART_STOPBITS_1;
	huartDebug.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	huartDebug.Init.Parity = UART_PARITY_NONE;
	huartDebug.Init.WordLength = UART_WORDLENGTH_8B;
	
	return HAL_UART_Init(&huartDebug);
#else
	return HAL_OK;
#endif
}

/*Retarget printf to SDEBUGER_UART_PORT*/
int fputc(int c, FILE *f) {
	while (HAL_UART_Transmit(&huartDebug, (uint8_t*) &c, 1, 10) != HAL_OK);
	return c;
}

/*Public functions*/

/*END OF FILE*/
